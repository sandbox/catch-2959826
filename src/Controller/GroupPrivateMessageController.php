<?php

namespace Drupal\group_private_message\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\group\Entity\Group;
use Drupal\private_message\Service\PrivateMessageServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class GroupPrivateMessageController extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The form builder interface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The private message service.
   *
   * @var \Drupal\private_message\Service\PrivateMessageServiceInterface
   */
  protected $privateMessageService;

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The http kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */

  /**
   * Constructs a PrivateMessageForm object.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\private_message\Service\PrivateMessageServiceInterface $privateMessageService
   *   The private message service.
   * @param \Symfony\Component\HttpFoundation\RequestInterface $request_stack
   *   The request.
   * @parm \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The http kernel.
   *
   */
  public function __construct(AccountProxyInterface $current_user, EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder, PrivateMessageServiceInterface $private_message_service, RequestStack $request_stack, HttpKernelInterface $http_kernel) {
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->privateMessageService = $private_message_service;
    $this->request = $request_stack->getCurrentRequest();
    $this->httpKernel = $http_kernel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('private_message.service'),
      $container->get('request_stack'),
      $container->get('http_kernel')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewThread(Group $group) {
    $private_message_thread = $group->private_message_thread->entity;

    $url = Url::fromRoute('entity.private_message_thread.canonical', ['private_message_thread' => $private_message_thread->id()]);
    $url->setAbsolute();
    $request = $this->request;

    $redirect_request = Request::create($url->toString(TRUE)->getGeneratedUrl(), 'GET', $request->query->all(), $request->cookies->all(), [], $request->server->all());
    // Carry over the session to the subrequest.
    if ($session = $request->getSession()) {
      $redirect_request->setSession($session);
    }
    $response = $this->httpKernel->handle($redirect_request, HttpKernelInterface::SUB_REQUEST);
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency($url);
    }
    return $response;
  }

}
